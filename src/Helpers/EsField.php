<?php
namespace PTT\Helpers;

use PTT\Helpers\Field;

class EsField extends Field {

    public function __toString() {
        return sprintf("this.%s", $this->name);
    }
}