<?php 
namespace PTT;
use PTT\Interfaces\Convert;

class Dump {
    public static function dump($filepath, $convert): bool {
        $input = $convert;
        if(is_writeable($filepath)) {
            if(is_string($convert)) {
                $input = $convert;
            } 
            else {
                if ($convert instanceof Convert) {
                    $input = $convert->convert();
                }
            }
            file_put_contents($filepath, $convert);
            return true;
        }
        return false;
    }
} 