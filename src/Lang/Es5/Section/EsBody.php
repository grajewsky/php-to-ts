<?php 


namespace PTT\Lang\Es5\Section;

use PTT\Interfaces\Section;
use PTT\Lang\DataLang\Section\Body;


class EsBody extends Body{
    protected $body  = "%s \nfunction %s%s(){\n\t%s\n}";
}