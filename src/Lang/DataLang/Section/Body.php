<?php 

namespace PTT\Lang\DataLang\Section;

use PTT\Interfaces\Section;


class Body implements Section {
    protected $childs = array();
    protected $body = " %s\nexport interface %s %s {\n\t%s\n} ";
    protected $extend = "extends %s";
    protected $import = "import { %s } from './%s';\n";
    protected $name = null;
    protected $extends;
    protected $imports = array();

    public function __construct(string $name, string $parent = null, array $imports = null) {
        $this->name = $name;
        $this->extends = ($parent == null) ? '' : sprintf($this->extend, $parent);
        $this->imports = ($imports == null) ? array(): $imports;
    }
    public function build(): string {
        $string = $this->body;
        $body = "";
        foreach($this->childs as $child) {
            $body .= sprintf("%s;\n\t", $child->build());
        }
        $imports = "";
        foreach(@$this->imports as $import ) {
            $imports .= sprintf($this->import, $import, $import);
        }   
        return sprintf($string, $imports, $this->name, $this->extends, $body);
    }
    public function append(Section $section) {
        array_push($this->childs, $section);
    }
}